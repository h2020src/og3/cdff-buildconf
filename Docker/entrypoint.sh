#!/bin/bash

mkdir -p /opt/cdff
cd /opt/cdff

if [ -e env.sh ]; then
	source env.sh
fi


# Replace this script by whatever command is given on the "docker run" command
# line or in the image's CMD instruction, in this order of precedence, without
# creating a new process. The script terminates and the command becomes the
# container's primary process.

exec "${@}"


