#!/bin/sh

docker build -t cdff:build_essential .

#start stage1 install
docker rm cdff_build_stage1
docker run -it -v $(readlink -f $SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent --name "cdff_build_stage1" cdff:build_essential /install_stage1.sh
docker commit cdff_build_stage1 cdff:build_stage1

#start stage2 install
docker rm cdff_build_stage2
docker run -it -v $(readlink -f $SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent --name "cdff_build_stage2" cdff:build_stage1 /install_stage2.sh
docker commit cdff_build_stage2 cdff:latest

#docker push cdff:latest
