#!/bin/bash

SHARE=$1
shift
PARAMS=$@

docker run -it -v $(readlink -f $SSH_AUTH_SOCK):/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent cdff:current --user=$USER:$UID --volume=$1:/opt/shared:r -w"/opt/shared" run /bin/bash

